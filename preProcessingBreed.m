% Author Ibrahim Shaer
% This function extracts the pureBred animals property from the Breed1 and
% Breed2 variables provided by the dataset. 
function pureBred = preProcessingBreed(data)
    pureBred = zeros(height(data), 1); 
    pureBreed1Indices = find(data.Breed1 ~= 0 & data.Breed2 == 0); 
    pureBreed2Indices = find(data.Breed1 == 0 & data.Breed2 ~= 0);
    
    pureBred([pureBreed1Indices;pureBreed2Indices]) = 1;
    pureBred = categorical(pureBred); 
end