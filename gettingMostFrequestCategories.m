function most_frequent_combo = gettingMostFrequestCategories(type_breed_combo_table, type)
    animal_type = 0; 
    if(type == 'dog')
        animal_type = 1; 
    else
        animal_type = 2;
    end
    
    only_animal_combo = type_breed_combo_table(type_breed_combo_table.Type == animal_type, 1); 
    h = histogram(only_animal_combo.breed_combo); 
    h.Normalization = 'probability'; 
    h.DisplayOrder = 'descend'; 
    h.NumDisplayBins = 5; 
    h.ShowOthers = 'on'; 
    if(type == 'cat')
        title('Most Common Cat Breed Combination'); 
    else
        title('Most Common Dog Breed Combination');
    end
    ylabel('Relative Frequency'); 
    xlabel('Breed Combinations');
    most_frequent_combo = h.Categories; 
    most_frequent_combo = categorical(string(most_frequent_combo)); 
    most_frequent_combo = most_frequent_combo';  
end