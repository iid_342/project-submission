% this function will result in a two column table. The first column
% is a logical representation of the photos existence. The second column is
% a logical representation of video existence. 
function media_files = media_files_extraction(data)
    media_files = table('Size', [height(data), 2], 'VariableNames', {'photo_files', 'video_files'}, 'VariableTypes', {'double', 'double'}); 
    
    photo_files = zeros(height(data),1);
    video_files = zeros(height(data), 1);
    
    photo_files_indices = find(data.PhotoAmt ~= 0); 
    video_files_indices = find(data.VideoAmt ~= 0); 
    
    photo_files(photo_files_indices) = 1; 
    video_files(video_files_indices) = 1; 
    
    media_files{:,:} = [photo_files video_files]; 
end