function random_forest_accuracy_table_1 = random_forest_1(for_training_X, for_training_Y, for_testing_X, for_testing_Y)
   
    random_forest_accuracy_table_1 = table('Size', [4, 3], 'VariableNames', {'NumTrees', 'MinLeafSize', 'Accuracy'}, 'VariableTypes', {'double', 'double', 'double'}); 
    index = 1;
    for j=9:2:15
        random_forest_model = TreeBagger(60, for_training_X, for_training_Y, 'Method', 'Classification', 'MinLeafSize', j, 'OOBVarImp', 'On', 'OOBPrediction', 'on', 'CategoricalPredictor', 'all'); 
        predicted_values = predict(random_forest_model, for_testing_X); 
        predicted_values = categorical(predicted_values); 
        C = confusionmat(for_testing_Y, predicted_values); 
        accuracy_result = (C(1,1) + C(2,2) + C(3,3) + C(4,4) + C(5,5)) / length(for_testing_Y);
        random_forest_accuracy_table_1{index, 1} = 60;
        random_forest_accuracy_table_1{index, 2} = j; 
        random_forest_accuracy_table_1{index, 3} = accuracy_result;
        index = index + 1; 
    end
end