TYPE 		--> 0 Cat , 1 Dog
Name 		--> 0 No name, 1 Has a name
Vaccinated	--> 1 Yes, 0 No
Vaccinated_NS	--> (Not sure if vaccinated) 1
Dewormed	--> 1 Yes, 0 No
Dewormed_NS	--> (Not sure if Dewormed) 1
Sterilized	--> 1 Yes, 0 No
Sterilized_NS	--> (Not sure if Sterilized) 1
Quantity	--> 0 Only one pet, 1 more than one pet. 
VDS_Combination	--> Combination of (Vaccinated, Dewormed, Sterilized, and their NS (Not Sure) 
	