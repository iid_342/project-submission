function [accuracy_result, C] = ensemble_learner(data)
    training_size = round(height(data)*0.8);
    data = [normalize(data(:, 1:end-1)) data(:, end)]; 
    
    for_training_X = data(1:training_size, 1:end-1); 
    for_training_Y = data(1:training_size, end);
    
    for_testing_X = data(training_size+1:end, 1:end-1);
    for_testing_Y = data(training_size+1:end, end);
    for_testing_Y = categorical(for_testing_Y.AdoptionSpeed);
    
    ensemble_learner_model = fitcensemble(for_training_X, for_training_Y, 'OptimizeHyperparameters', 'all');
    
    predicted_values = predict(ensemble_learner_model, for_testing_X); 
    predicted_values = categorical(predicted_values); 
    C = confusionmat(for_testing_Y, predicted_values); 
    accuracy_result = (C(1,1) + C(2,2) + C(3,3) + C(4,4) + C(5,5)) / length(for_testing_Y);
end