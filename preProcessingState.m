% This function maps the State variable provided by the state to an ordinal
% variable, in the 1-5 range, that represents the average income of a
% person in this state. 
function statePreprocessed = preProcessingState(data, gdpperstate)
  statePreprocessed = zeros(height(data), 1);
  for i=1:height(data) 
    state_train = data(i, :).State; 
    statePreprocessed(i) = gdpperstate(gdpperstate.StateID == state_train, :).Type; 
  end
  statePreprocessed = categorical(statePreprocessed);   
end