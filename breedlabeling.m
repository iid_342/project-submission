function breed_labels = breedlabeling(type_breed_combo_table)
    most_frequent_dog_combo = gettingMostFrequestCategories(type_breed_combo_table, 'dog');
    most_frequent_cat_combo = gettingMostFrequestCategories(type_breed_combo_table, 'cat'); 
    
    VariableDogNames = {'dog_breed_1', 'dog_breed_2', 'dog_breed_3', 'dog_breed_4', 'dog_breed_5', 'dog_breed_6'}; 
    VariableCatNames = {'cat_breed_1', 'cat_breed_2', 'cat_breed_3', 'cat_breed_4', 'cat_breed_5', 'cat_breed_6'};
    VariableTypes = {'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double'};
    breed_labels = table('Size', [height(type_breed_combo_table) 12], 'VariableNames', [VariableDogNames VariableCatNames], 'VariableTypes', VariableTypes); 
    
    % label most frequent dogs 
    other_dog_labels = []; 
    only_dog_indices = find(type_breed_combo_table.Type == 1); 
    for i=1:length(most_frequent_dog_combo)
        dog_combo = most_frequent_dog_combo(i); 
        dog_combo_indices = find(type_breed_combo_table.breed_combo == dog_combo & type_breed_combo_table.Type == 1); 
        other_dog_labels = [other_dog_labels; dog_combo_indices]; 
        breed_labels{dog_combo_indices, i} = 1; 
    end
    breed_labels{other_dog_labels, 6} = 1; 
    breed_labels{only_dog_indices, 6} = not(breed_labels{only_dog_indices, 6}); 
    
    other_cat_labels = []; 
    only_cat_indices = find(type_breed_combo_table.Type == 2); 
    for i=1:length(most_frequent_cat_combo)
        cat_combo = most_frequent_cat_combo(i); 
        cat_combo_indices = find(type_breed_combo_table.breed_combo == cat_combo & type_breed_combo_table.Type == 2); 
        other_cat_labels = [other_cat_labels; cat_combo_indices]; 
        breed_labels{cat_combo_indices, 6+i} = 1; 
    end
    breed_labels{other_cat_labels, 12} = 1; 
    breed_labels{only_cat_indices, 12} = not(breed_labels{only_cat_indices, 12}); 
end