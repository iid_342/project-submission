function [Breed1_names, Breed2_names, Breed_combination] = gettingBreedNames(data, breedlabels)
    Breed1_names = cell(height(data), 1); 
    Breed2_names = cell(height(data), 1);
    Breed_combination = cell(height(data), 1);
    for i=1:height(data)
        breed1_id = data(i, :).Breed1;
        breed2_id = data(i, :).Breed2;
        if(breed1_id ~= 0)
            Breed1_names{i} = cellstr(breedlabels(breedlabels.BreedID == breed1_id, :).BreedName);
        else 
            Breed1_names{i} = {''}; 
        end
        
        if(breed2_id ~= 0)
            Breed2_names{i} = cellstr(breedlabels(breedlabels.BreedID == breed2_id, :).BreedName);
        else 
            Breed2_names{i} = {''}; 
        end
        Breed_combination{i} = strcat(Breed1_names{i}, '__', Breed2_names{i});
    end
end