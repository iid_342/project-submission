% implementation of random forest using grid search for different values of
% the number of trees. The method returns a table with accuracy values

function [random_forest_accuracy_table, all_confusion_matrices] = random_forest(for_training_X, for_training_Y, for_testing_X, for_testing_Y, nbCat)
    
    all_confusion_matrices = [];
    random_forest_accuracy_table = table('Size', [56, 3], 'VariableNames', {'NumTrees', 'MinLeafSize', 'Accuracy'}, 'VariableTypes', {'double', 'double', 'double'}); 
    index = 1; 
    
    for i=5:5:70
        for j=1:2:7
            random_forest_model = TreeBagger(i, for_training_X, for_training_Y, 'Method', 'Classification', 'MinLeafSize', j, 'OOBVarImp', 'On', 'OOBPrediction', 'on', 'CategoricalPredictor', 'all'); 
            predicted_values = predict(random_forest_model, for_testing_X); 
            predicted_values = categorical(predicted_values); 
            C = confusionmat(for_testing_Y, predicted_values); 
            all_confusion_matrices = [all_confusion_matrices; C]; 
            if(nbCat == 5)
                accuracy_result = (C(1,1) + C(2,2) + C(3,3) + C(4,4) + C(5,5)) / length(for_testing_Y);
            else
                accuracy_result = (C(1,1) + C(2,2) + C(3,3)) / length(for_testing_Y);
            end
            random_forest_accuracy_table{index, 1} = i;
            random_forest_accuracy_table{index, 2} = j; 
            random_forest_accuracy_table{index, 3} = accuracy_result;
            index = index + 1; 
        end
    end

end