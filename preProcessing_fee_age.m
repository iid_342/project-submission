function new_age_fee = preProcessing_fee_age(data)
    new_age_fee = table('Size', [height(data) 2], 'VariableTypes', {'double', 'double'}, 'VariableNames', {'Age', 'Fee'});
    
    new_age_feature = nthroot(data.Age, 3);
    new_fee_feature = nthroot(data.Fee, 3); 
    
    new_age_fee.Age = new_age_feature; 
    new_age_fee.Fee = new_fee_feature; 
end

