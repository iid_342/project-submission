% Author Ibrahim Shaer
% This function extracts information from the fee column provided by the
% dataset. Considering that the fee of adopting a pet is a continous value,
% we map the fee variable to an ordinal variable in the 0-3 range. 
function feePreprocessed = preProcessingFee(data)
    feePreprocessed = zeros(height(data), 1);
    range_1_fees = find(data.Fee > 0 & data.Fee <= 100);
    range_2_fees = find(data.Fee > 100 & data.Fee <= 500);
    range_3_fees = find(data.Fee > 500); 
    feePreprocessed(range_1_fees) = 1;
    feePreprocessed(range_2_fees) = 2;
    feePreprocessed(range_3_fees) = 3;
    feePreprocessed = categorical(feePreprocessed);
end